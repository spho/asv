package com.example.christoph.asv;

import java.util.TimerTask;

/**
 * Created by Christoph on 28.09.2015.
 */
public class MasterClock extends TimerTask {
    Coordinator coordinator;
    int purpose = 0;

    public MasterClock(Coordinator cor, int purp){
        coordinator=cor;
        purpose=purp;
    }

    public void run() {
        if(purpose==0) {
            coordinator.mHandler.obtainMessage(5, 7, 1, null)
                    .sendToTarget();
        }
        if(purpose==1) {
            coordinator.mHandler.obtainMessage(9, 7, 1, null)
                    .sendToTarget();
        }
    }
}
