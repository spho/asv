package com.example.christoph.asv;

import java.util.List;
import java.util.ArrayList;

public class ASV {

    //Speed of Motors
    public float speedM1 = 0;
    public float speedM2 = 0;

    //Torques of Motors
    public float torqueLeft=0;
   public float torqueRight=0;

    //Speed of vehicle and angle of the front joint
    public float speedASV = 0;
    public float angleASV = 0;

    //Energy values
    public float battery = 70;
    public float capacitor = 0;
    public float pv = 0;
    public float onBoardElectronics = 5;
    public float powerM1 = 0;
    public float powerM2 = 0;
    public float[] energyflow= new float[100];

    //Sensor values, to be annaunced in more detail later
    public float[] sensor= new float[100];

    //Pathpoints
    public Pathpoint[] pathpoint= new Pathpoint[100];
 Pathpoint positionASV = new Pathpoint(40,50);
 Pathpoint[] quadASV = new Pathpoint[3];

    //Recording
    public ArrayList<ArrayList<Byte>> recordings = new ArrayList<ArrayList<Byte>>();
    public int bufferFillRate = 0;
    public boolean isRecording = false;
    public boolean isSync = false;

    //Debugmode
    public String request = "";
    public String reply = "";

    public float inputSpeed = 0;
    public float inputSteering = 0;


    //Error
    public boolean hasError = false;

    public ASV() {
     quadASV[0]=new Pathpoint(10,10);
     quadASV[1]=new Pathpoint(40,50);
     quadASV[2]=new Pathpoint(40,90);
    }


}


