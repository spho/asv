package com.example.christoph.asv;

import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MenuActivity extends  SuperActivity {

    Button[] buttons = new Button[6];
    private View.OnClickListener clicklist;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        init();
        initUI();
    }

    @Override
    int setID() {
        return 7;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    void updateUI(Datapacket dp) {

    }

    @Override
    void initUI() {

        clicklist =  new View.OnClickListener() {
            @Override
            public void onClick(View button) {
                Log.d("Menu","Pressed" +button.toString());
                if(button==buttons[0]){
                    Log.d("Menu","Button0 pressed");
                    sendToCordinator(4, 1, -1, null);
                }
                if(button==buttons[1]){
                    sendToCordinator(4,2,-1,null);
                }
                if(button==buttons[2]){
                    sendToCordinator(4,3,-1,null);
                }
                if(button==buttons[3]){
                    sendToCordinator(4,4,-1,null);
                }
                if(button==buttons[4]){
                    sendToCordinator(4,5,-1,null);
                }
                if(button==buttons[5]){
                    sendToCordinator(4,6,-1,null);
                }


            }
        };


        buttons[0]=(Button) findViewById(R.id.button8);
        buttons[1]=(Button) findViewById(R.id.button7);
        buttons[2]=(Button) findViewById(R.id.button6);
        buttons[3]=(Button) findViewById(R.id.button5);
        buttons[4]=(Button) findViewById(R.id.button4);
        buttons[5]=(Button) findViewById(R.id.button3);

        buttons[0].setOnClickListener(clicklist);
        buttons[1].setOnClickListener(clicklist);
        buttons[2].setOnClickListener(clicklist);
        buttons[3].setOnClickListener(clicklist);
        buttons[4].setOnClickListener(clicklist);
        buttons[5].setOnClickListener(clicklist);


    }


}
