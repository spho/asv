package com.example.christoph.asv;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

/**
 * Created by Christoph on 09.10.2015.
 */

public class EnergyView extends View {

    Bitmap backgroundBitmap;
    Paint paint;
    int width;
    int height;
    int actionBarHeight;
    float[][] energyFlows = new float[12][3];

    private boolean initedAlreadyIMeanSeriouslyWhyDoesThisBeCalledTwoTimesWhereIsTheLocigSerisouslyUKiddingARRRRG=false;


    final int orginalX = 810;
    final int orginalY = 460;

    final int numberOfTextPoints = 12;
    String[] lableTexts = new String[numberOfTextPoints];
    final float textSize = 30f;
    final float[] orginalLablelX = {30f/orginalX,30f/orginalX,30f/orginalX, 256f/orginalX,256f/orginalX,256f/orginalX, 680f/orginalX,680f/orginalX,680f/orginalX,130f/orginalX,360f/orginalX,600f/orginalX};
    final float[] orginalLablelY = {130f/orginalY,(130f+textSize*0.6f)/orginalY,(130f+2*textSize*0.6f)/orginalY, 20f/orginalY,(20f+textSize*0.6f)/orginalY,(20f+2*textSize*0.6f)/orginalY, 130f/orginalY,(130f+textSize*0.6f)/orginalY,(130f+2*textSize*0.6f)/orginalY,350f/orginalY,350f/orginalY,350f/orginalY};

    public EnergyView(Context context) {
        super(context);
        initEnergyView(context);
    }

    public EnergyView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initEnergyView(context);
    }

    public EnergyView(Context context, AttributeSet attrs, int defaultStyle) {
        super(context, attrs, defaultStyle);
        initEnergyView(context);
    }

    public void initEnergyView(Context context) {

        if(!initedAlreadyIMeanSeriouslyWhyDoesThisBeCalledTwoTimesWhereIsTheLocigSerisouslyUKiddingARRRRG) {
            for (int i = 0; i < 12; i++) {
                for (int j = 0; j < 3; j++) {
                    energyFlows[i][j] = 0;
                }
            }


            WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            // Calculate ActionBar height
            TypedValue tv = new TypedValue();
            if (context.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
                actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
            }

            width = size.x;
            height = size.y - actionBarHeight;


            Log.d("EnergyView", "Measured width: " + width + " measuerd height: " + height);

            paint = new Paint();
            paint.setTextSize(textSize);

        /*
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.energymap);
        backgroundBitmap = Bitmap.createScaledBitmap(bm, width, height, false);
*/
            for (int i = 0; i < numberOfTextPoints; i++) {
                orginalLablelX[i] *= width;
                orginalLablelY[i] *= height;
                lableTexts[i] = 2 * i+"";
            }
            initedAlreadyIMeanSeriouslyWhyDoesThisBeCalledTwoTimesWhereIsTheLocigSerisouslyUKiddingARRRRG=true;
        }
    }



    public void update(float[] floats){

           lableTexts[0]=floats[0]+"W";
        lableTexts[1]=floats[1]+"V";
        lableTexts[2]=floats[2]+"%";
        lableTexts[3]=floats[3]+"W";
        lableTexts[4]=floats[4]+"V";
        lableTexts[5]=floats[5]+"%";
        lableTexts[6]=floats[6]+"W";
        lableTexts[7]=floats[7]+"V";
        lableTexts[8]=floats[8]+"%";
        lableTexts[9]=floats[9]+"W";
        lableTexts[10]=floats[10]+"W";
        lableTexts[11]=floats[11]+"W";


    }


    @Override
    protected void onDraw(Canvas canvas) {
        //canvas.drawColor(Color.GRAY);
        // canvas.drawBitmap(backgroundBitmap, 0, 0, paint);


        for (int i = 0; i < numberOfTextPoints; i++) {
            canvas.drawText(lableTexts[i]+"", orginalLablelX[i], orginalLablelY[i], paint);
        }


    }

}
