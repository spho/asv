package com.example.christoph.asv;

import android.os.Handler;
import android.os.Message;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends SuperActivity {


    private Button button;
    private Button button2;
    private TextView[] textview = new TextView[20];
    private OnClickListener clicklist;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
        initUI();
    }

    void initUI(){
        clicklist =  new OnClickListener() {
            @Override
            public void onClick(View arg0){
                if (arg0 == button) {
                    button.setText("Clicket");
                    Datapacket datap = new Datapacket();
                    Log.d("Handler", "Bevore sending");
                    coordinator.mHandler.obtainMessage(2, 1, 1, null)
                            .sendToTarget();
                }
                if (arg0 == button2) {
                    sendSteeringInput(1.34f + counter, 2.43f - counter);
                    counter++;
                }
            }

        };

    }

    @Override
    int setID() {
        return 9;
    }



     void updateUI(Datapacket dp) {
        int lenght = dp.dataArray.length;
        switch (currentView) {
            case 1:
                for (int i = 0; i < lenght; i++) {
                    if (dp.dataArray[i].type == 3) {
                        textview[0].setText("Speed is " + dp.dataArray[i].floaty);
                    }
                }

                break;
            case 2:
                break;
            default:
                break;
        }
    }

    public void addListenerOnButton() {

        button = (Button) findViewById(R.id.button);
        button2 = (Button) findViewById(R.id.button2);
        button.setOnClickListener(clicklist);
        button2.setOnClickListener(clicklist);

    }






    public void sendSteeringInput(float v1, float v2) {
        Datapacket dp = new Datapacket();
        dp.addValue(1, v1);
        dp.addValue(2, v2);
        dp.fialiseArray();
        sendToCordinator(2,1,1,dp);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        sendToCordinator(4,1,1,null);

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
