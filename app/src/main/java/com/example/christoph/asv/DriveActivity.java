package com.example.christoph.asv;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

public class DriveActivity extends SuperActivity {



    JoystickView joystick;

    int debugCounter = 0;
    int debugCounter2 = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drive);
        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setCustomView(R.layout.myfancycustomheader);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.LTGRAY));
        errorButton = (Button) findViewById(R.id.button15);
        driveButton = (Button) findViewById(R.id.button17);
        driveplaningButton = (Button) findViewById(R.id.button9);
        batteryButton = (Button) findViewById(R.id.button16);




        View.OnTouchListener onTouchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getActionMasked() ==  MotionEvent.ACTION_UP) {
                    Log.d("Click", "Clicked on: " + v.toString());
                    if (v == (View) errorButton) {
                        sendToCordinator(4, 6, 1, null);
                    }
                    if (v == (View) batteryButton) {
                        sendToCordinator(4, 2, 1, null);
                    }
                    if (v == (View) driveplaningButton) {
                        sendToCordinator(4, 4, 1, null);
                    }
                    if (v == (View) driveButton) {
                        sendToCordinator(4, 1, 1, null);
                    }



                }
                return true;
            }
        };

        errorButton.setOnTouchListener(onTouchListener);
        batteryButton.setOnTouchListener(onTouchListener);
      // driveButton.setOnTouchListener(onTouchListener);
        driveplaningButton.setOnTouchListener(onTouchListener);

        initUI();



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_drive, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        sendToCordinator(4, 2, 1, null);
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    int setID() {
        return 1;
    }

    @Override
    void updateUI(Datapacket dp) {



        float energyfloaty = 50;
        try {
            for (int i = 0; i < dp.dataArray.length; i++) {
                if (dp.dataArray[i].type == 5) {
                  //  dynamicMeterForEnergy.update((int) dp.dataArray[i].floaty);
                } else if (dp.dataArray[i].type == 7) {
                    energyfloaty -= dp.dataArray[i].floaty;
                } else if (dp.dataArray[i].type == 8) {
                    energyfloaty += dp.dataArray[i].floaty;
                } else if (dp.dataArray[i].type == 9) {
                    energyfloaty += dp.dataArray[i].floaty;
                } else if (dp.dataArray[i].type == 10) {
                    energyfloaty += dp.dataArray[i].floaty;
                }


            }
            //Log.d("Drive", "energyfloaty: " + energyfloaty);
        //    dynamicMeterForConsumption.update((int) energyfloaty);

        } catch (
                Exception e
                )

        {

        }

        // dynamicMeterForEnergy.update(80);
        //  dynamicMeterForConsumption.update(90);
/*
        layoutForDynamicMeterForEnergy.removeAllViews();
        layoutForDynamicMeterForEnergy.refreshDrawableState();
        layoutForDynamicMeterForEnergy.addView(dynamicMeterForEnergy);

        layoutForDynamicMeterForConsumption.removeAllViews();
        layoutForDynamicMeterForConsumption.refreshDrawableState();
        layoutForDynamicMeterForConsumption.addView(dynamicMeterForConsumption);
*/
    }

    @Override
    void initUI() {
        // add Dynamic meter to pre-defined layout
        /*
        dynamicMeterForEnergy = new DynamicMeter(this, 30, 1);
        layoutForDynamicMeterForEnergy = (ViewGroup) findViewById(R.id.energySliderLayout);
        layoutForDynamicMeterForEnergy.addView(dynamicMeterForEnergy);

        dynamicMeterForConsumption = new DynamicMeter(this, 30, 2);
        layoutForDynamicMeterForConsumption = (ViewGroup) findViewById(R.id.powerSliderLayout);
        layoutForDynamicMeterForConsumption.addView(dynamicMeterForConsumption);
*/
        //Referencing also other views
        joystick = (JoystickView) findViewById(R.id.joystickview);
        joystick.setDriveActivity(this);

        //Event listener that always returns the variation of the angle in degrees, motion power in percentage and direction of movement
        joystick.setOnJoystickMoveListener(new JoystickView.OnJoystickMoveListener() {

            @Override
            public void onValueChanged(int angle, int power, int direction) {
                // TODO Auto-generated method stub
                Datapacket dp = new Datapacket();
                dp.addValue(20,(float)power);
                dp.addValue(21, (float)(int) angle);
                dp.fialiseArray();
                sendToCordinator(1, 1, 1, dp);
                //Log.d("COM","Send steeriing input to coordinator from drive activity, "+debugCounter+"  "+power+" "+angle);
                debugCounter++;
            }
        }, JoystickView.DEFAULT_LOOP_INTERVAL);


    }


}
