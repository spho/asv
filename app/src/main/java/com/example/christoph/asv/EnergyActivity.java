package com.example.christoph.asv;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

public class EnergyActivity extends SuperActivity {

    PieChart myFavouritePie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_energy);
        initUI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_energy, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        sendToCordinator(4, 7, 1, null);
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    void updateUI(Datapacket dp) {
        float[] floats = new float[6];

        for (int i = 0; i < dp.dataArray.length; i++) {
            if (dp.dataArray[i].type == 5) {
               // floats[0] = dp.dataArray[i].floaty;
                floats[0] =0;

            } else if (dp.dataArray[i].type == 6) {
                floats[1] = dp.dataArray[i].floaty;
            } else if (dp.dataArray[i].type == 7) {
                //floats[2] = dp.dataArray[i].floaty;
                floats[2] =0;
            } else if (dp.dataArray[i].type == 8) {
                floats[3] = dp.dataArray[i].floaty;
            } else if (dp.dataArray[i].type == 9) {
                floats[4] = dp.dataArray[i].floaty;
            } else if (dp.dataArray[i].type == 10) {
                floats[5] = dp.dataArray[i].floaty;
            }
        }
        myFavouritePie.update(floats);
        myFavouritePie.invalidate();
    }

        @Override
        void initUI(){

            final android.support.v7.app.ActionBar actionBar = getSupportActionBar();
            actionBar.setCustomView(R.layout.myfancycustomheader);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setBackgroundDrawable(new ColorDrawable(Color.LTGRAY));
            errorButton = (Button) findViewById(R.id.button15);
            driveButton = (Button) findViewById(R.id.button17);
            driveplaningButton = (Button) findViewById(R.id.button9);
            batteryButton = (Button) findViewById(R.id.button16);


            View.OnTouchListener onTouchListener = new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if(event.getActionMasked() ==  MotionEvent.ACTION_UP) {
                        Log.d("Click", "Clicked on: " + v.toString());
                        if (v == (View) errorButton) {
                            sendToCordinator(4, 6, 1, null);
                        }
                        if (v == (View) batteryButton) {
                            sendToCordinator(4, 2, 1, null);
                        }
                        if (v == (View) driveplaningButton) {
                            sendToCordinator(4, 4, 1, null);
                        }
                        if (v == (View) driveButton) {
                            sendToCordinator(4, 1, 1, null);
                        }
                    }
                    return true;
                }
            };
            errorButton.setOnTouchListener(onTouchListener);
            //batteryButton.setOnTouchListener(onTouchListener);
            driveButton.setOnTouchListener(onTouchListener);
            driveplaningButton.setOnTouchListener(onTouchListener);

            myFavouritePie = (PieChart) findViewById(R.id.pieChart);



        }

        @Override
        int setID () {
            return 2;
        }
    }
