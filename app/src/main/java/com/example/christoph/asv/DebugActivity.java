package com.example.christoph.asv;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Scroller;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class DebugActivity extends SuperActivity {


    TextView textView;
    String sendBuffer = "0";
    String consoleString = "Console Started";


    String filePath = "";

   String lastmsg="";

    @Override
    protected void onPause() {
        super.onPause();
        writeToFile(consoleString);
    }

   @Override
    protected void onResume() {
        super.onResume();
        consoleString += readFromFile();
       textView.setText(consoleString);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debug);

        filePath = "configDebug.txt";
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        //textView.requestFocus();


        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setCustomView(R.layout.myfancycustomheader);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.LTGRAY));
        errorButton = (Button) findViewById(R.id.button15);
        driveButton = (Button) findViewById(R.id.button17);
        driveplaningButton = (Button) findViewById(R.id.button9);
        batteryButton = (Button) findViewById(R.id.button16);


        View.OnTouchListener onTouchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getActionMasked() == MotionEvent.ACTION_UP) {
                    Log.d("Click", "Clicked on: " + v.toString());
                    if (v == (View) errorButton) {
                        sendToCordinator(4, 6, 1, null);
                    }
                    if (v == (View) batteryButton) {
                        sendToCordinator(4, 2, 1, null);
                    }
                    if (v == (View) driveplaningButton) {
                        sendToCordinator(4, 4, 1, null);
                    }
                    if (v == (View) driveButton) {
                        sendToCordinator(4, 1, 1, null);
                    }
                    /*if (v == (View) sendButton) {
                        try {
                            int command = Integer.parseInt(editText.getText().toString());
                            coordinator.mHandler.obtainMessage(7, 1, command, null)
                                    .sendToTarget();
                        } catch (Exception e) {
                            Toast.makeText(getApplicationContext(), "Input wrong formatted",
                                    Toast.LENGTH_LONG).show();
                        }
                    }*/
                }
                return true;
            }
        };
        // errorButton.setOnTouchListener(onTouchListener);
        batteryButton.setOnTouchListener(onTouchListener);
        driveButton.setOnTouchListener(onTouchListener);
        driveplaningButton.setOnTouchListener(onTouchListener);


        textView = (TextView) findViewById(R.id.textView3);
        textView.setScroller(new Scroller(this));
        textView.setMaxLines(9999);
        textView.setSingleLine(false);
        textView.setVerticalScrollBarEnabled(true);
        textView.setMovementMethod(new ScrollingMovementMethod());


        textView.setText(consoleString);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_debug, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.button15) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    void updateUI(Datapacket dp) {
        for (int i = 0; i < dp.dataArray.length; i++) {
            if (dp.dataArray[i].type == 19) {
                // textView.setText(dp.dataArray[i].text);
                if (dp.dataArray[i].text!=lastmsg) {
                    consoleString += "\r\n" + dp.dataArray[i].text;
                    textView.setText(consoleString);
                    lastmsg=dp.dataArray[i].text;
                }
            }
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

    }


    @Override
    void initUI() {

    }

    @Override
    int setID() {
        return 6;
    }


    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        String addString = "";
        switch (keyCode) {

            case KeyEvent.KEYCODE_0:
                addString = "0";
                break;
            case KeyEvent.KEYCODE_1:
                addString = "1";
                break;
            case KeyEvent.KEYCODE_2:
                addString = "2";
                break;
            case KeyEvent.KEYCODE_3:
                addString = "3";
                break;
            case KeyEvent.KEYCODE_4:
                addString = "4";
                break;
            case KeyEvent.KEYCODE_5:
                addString = "5";
                break;
            case KeyEvent.KEYCODE_6:
                addString = "6";
                break;
            case KeyEvent.KEYCODE_7:
                addString = "7";
                break;
            case KeyEvent.KEYCODE_8:
                addString = "8";
                break;
            case KeyEvent.KEYCODE_9:
                addString = "9";
                break;
            case KeyEvent.KEYCODE_A:
                addString = "A";
                break;
            case KeyEvent.KEYCODE_B:
                addString = "B";
                break;
            case KeyEvent.KEYCODE_C:
                addString = "C";
                break;
            case KeyEvent.KEYCODE_D:
                addString = "D";
                break;
            case KeyEvent.KEYCODE_E:
                addString = "E";
                break;
            case KeyEvent.KEYCODE_F:
                addString = "F";
                break;
            case KeyEvent.KEYCODE_G:
                addString = "G";
                break;
            case KeyEvent.KEYCODE_H:
                addString = "H";
                break;
            case KeyEvent.KEYCODE_I:
                addString = "I";
                break;
            case KeyEvent.KEYCODE_J:
                addString = "J";
                break;
            case KeyEvent.KEYCODE_K:
                addString = "K";
                break;
            case KeyEvent.KEYCODE_L:
                addString = "L";
                break;
            case KeyEvent.KEYCODE_M:
                addString = "M";
                break;
            case KeyEvent.KEYCODE_N:
                addString = "N";
                break;
            case KeyEvent.KEYCODE_O:
                addString = "O";
                break;
            case KeyEvent.KEYCODE_P:
                addString = "P";
                break;
            case KeyEvent.KEYCODE_Q:
                addString = "Q";
                break;
            case KeyEvent.KEYCODE_R:
                addString = "R";
                break;
            case KeyEvent.KEYCODE_S:
                addString = "S";
                break;
            case KeyEvent.KEYCODE_T:
                addString = "T";
                break;
            case KeyEvent.KEYCODE_U:
                addString = "U";
                break;
            case KeyEvent.KEYCODE_V:
                addString = "V";
                break;
            case KeyEvent.KEYCODE_W:
                addString = "W";
                break;
            case KeyEvent.KEYCODE_X:
                addString = "X";
                break;
            case KeyEvent.KEYCODE_Y:
                addString = "Y";
                break;
            case KeyEvent.KEYCODE_Z:
                addString = "Z";
                break;


            case KeyEvent.KEYCODE_ENTER:

                try {
                    int command = Integer.parseInt(sendBuffer);
                    switch(command){
                        case 999:
                            consoleString="";

                            sendBuffer = "";
                            textView.setText("");
                            return super.onKeyUp(keyCode, event);


                        default:

                            coordinator.mHandler.obtainMessage(7, 1, command, null)
                                    .sendToTarget();
                            break;
                    }

                } catch (Exception e) {
                    sendBuffer = "Incorrect input formatting";
                }

                consoleString = consoleString + "\r\n" + sendBuffer;
                sendBuffer = "";
                break;
            default:
                return super.onKeyUp(keyCode, event);
        }


        sendBuffer += addString;
        textView.setText(consoleString + "\r\n" + sendBuffer);

        final int scrollAmount = textView.getLayout().getLineTop(textView.getLineCount()) - textView.getHeight();
        // if there is no need to scroll, scrollAmount will be <=0
        if (scrollAmount > 0)
            textView.scrollTo(0, scrollAmount);
        else
            textView.scrollTo(0, 0);


        return true;
    }


    private void writeToFile(String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput( filePath, Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }


    private String readFromFile() {

        String ret = "";


        try {


           /* File f = new File(filePath);

            if (f.exists() == false) {
                f.createNewFile();
            }*/

            InputStream inputStream = openFileInput(filePath);

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }

}
