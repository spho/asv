package com.example.christoph.asv;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

/**
 * Created by Christoph on 26.10.2015.
 */
public class PieChart extends View {

    private int nrOfSegments = 6;
    private float[] energyConsumtions = new float[nrOfSegments];
    private float overallConsumation = 0;

    float maxSumofEnergies = 1500;

    int width;
    int height;

    int midX;
    int midY;

    int actionBarHeight;
    boolean safeGuard = false;
    RectF disp;
    Paint paint = new Paint();

    public PieChart(Context context) {
        super(context);
        initView(context);
    }

    public PieChart(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public PieChart(Context context, AttributeSet attrs, int defaultStyle) {
        super(context, attrs, defaultStyle);
        initView(context);
    }

    private float returnSumOfConsumation() {
        float floaty = 0;
        for (int i = 0; i < energyConsumtions.length; i++) {
            floaty += energyConsumtions[i];
        }
        return floaty;
    }

    private void initView(Context context) {

        if (!safeGuard) {

            WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            // Calculate ActionBar height
            TypedValue tv = new TypedValue();
            if (context.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
                actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
            }

            energyConsumtions[0] = 50;
            energyConsumtions[1] = 50;
            energyConsumtions[2] = 50;
            energyConsumtions[3] = 50;
            energyConsumtions[4] = 50;
            energyConsumtions[5] = 50;
            overallConsumation = returnSumOfConsumation();

            width = size.x;
            height = size.y - actionBarHeight;

            disp = new RectF(0, 0, width - 20, width - 20);
            paint.setColor(Color.BLUE);
            paint.setAntiAlias(true);

            safeGuard = false;
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        width = (int) this.getX();
        height = (int) this.getY();
        disp = new RectF(0, 0, width - 20, width - 20);
    }

    @Override
    protected void onSizeChanged(int xNew, int yNew, int xOld, int yOld) {
        super.onSizeChanged(xNew, yNew, xOld, yOld);
        width = xNew;
        height = yNew;

        midX = width/2;
        midY = height/2;

        disp = new RectF(0, 0, xNew, xNew);

    }

    public void update(float[] values) {
        energyConsumtions = new float[values.length];
        for (int i = 0; i < values.length; i++) {
            energyConsumtions[i] = values[i];
        }
        overallConsumation = returnSumOfConsumation();
    }


    private float[] calcPieces() {
        float[] floaty = new float[energyConsumtions.length * 2];

        float zwival = 0;

        for (int i = 0; i < energyConsumtions.length; i++) {
            floaty[2 * i] = zwival;
            float increment =  energyConsumtions[i] * 360 / overallConsumation;
            floaty[2 * i + 1] = increment;
            zwival = increment+zwival;
        }

        return floaty;
    }


    @Override
    protected void onDraw(Canvas canvas) {
        //canvas.drawColor(Color.GRAY);
        // canvas.drawBitmap(backgroundBitmap, 0, 0, paint);

        float x = Math.max(0, Math.min(width / 2 - width / 2 * overallConsumation / maxSumofEnergies, width / 2 - 20));
        float y = Math.max(0, Math.min(height / 2 - width / 2 * overallConsumation / maxSumofEnergies, width / 2 - 20));
        float r = Math.max(40, Math.min(width / 2, width * overallConsumation / maxSumofEnergies));


        disp = new RectF(midX-r, midX-r, r + midX, r + midX);

        Paint[] paints = new Paint[6];

        for(int i = 0;i<6;i++) {
            paints[i] = new Paint();
        }

        paints[0].setColor(Color.GRAY);
        paints[1].setColor(Color.YELLOW);
        paints[2].setColor(Color.MAGENTA);
        paints[3].setColor(Color.GREEN);
        paints[4].setColor(Color.BLUE);
        paints[5].setColor(Color.RED);



            float[] floatys = calcPieces();

           // canvas.drawArc(disp,0,360,true,paints[0]);

            for (int j = 0; j < floatys.length / 2; j++) {
                canvas.drawArc(disp, floatys[2 * j], floatys[2 * j + 1], true, paints[j%6]);

            }





    }



}
