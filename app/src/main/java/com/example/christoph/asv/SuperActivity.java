package com.example.christoph.asv;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

/**
 * Created by Christoph on 28.09.2015.
 */
abstract class SuperActivity extends AppCompatActivity {

    protected Coordinator coordinator;
    protected int counter = 0;
    protected boolean firstStart = true;
    public Handler myHandler;
    protected int currentView = 1;
    Button errorButton;
    Button batteryButton;
    Button driveplaningButton;
    Button driveButton;


    public int idOfActivity = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
        coordinator.mHandler.obtainMessage(6, -1, -1, this)
                .sendToTarget();
    }


    protected void init() {
        if (firstStart) {
            idOfActivity = setID();
            myHandler = new Handler() {
                public void handleMessage(Message msg) {
                    receivedMsg(msg);
                }
            };
            firstStart = false;
        }
    }

    protected void sendToCordinator(int what, int arg1, int arg2, Datapacket dp) {
        coordinator.mHandler.obtainMessage(what, arg1, arg2, dp)
                .sendToTarget();

    }

    public void receiveCoordinator(Coordinator cord) {
        coordinator = cord;
        coordinator.currentContext = this;
    }


    protected void receivedMsg(Message msg) {
        switch (msg.what) {
            case 3:
                try {
                    if (msg.obj != null) {

                        Datapacket sp = (Datapacket) msg.obj;
                        updateUI((Datapacket) sp);
                        for (int i = 0; i < sp.dataArray.length; i++) {
                            if (sp.dataArray[i].type == 5) {
                                updateEnergyStatus((int) (sp.dataArray[i].floaty/95*3));
                            }
                            if(sp.dataArray[i].type==23) {
                                if (sp.dataArray[i].bool == true) {
                                    errorButton.setBackgroundResource(R.drawable.chatbox_error);
                                } else {
                                   errorButton.setBackgroundResource(R.drawable.chatbox);

                                }
                            }
                        }
                    }
                } catch (Exception e) {

                }
                break;
            default:
                break;
        }
    }





    protected void updateEnergyStatus(int status) {
        if (batteryButton != null) {
            switch (status) {
                case 0:
                    batteryButton.setBackgroundResource(R.drawable.battery0);
                    break;
                case 1:
                    batteryButton.setBackgroundResource(R.drawable.battery1);
                    break;
                case 2:
                    batteryButton.setBackgroundResource(R.drawable.battery2);
                    break;
                case 3:
                    batteryButton.setBackgroundResource(R.drawable.battery3);
                    break;
                case 4:
                   // batteryButton.setBackgroundResource(R.drawable.battery4);
                    break;
                case 5:
                 //   batteryButton.setBackgroundResource(R.drawable.battery5);
                    break;
                case 6:
                 //   batteryButton.setBackgroundResource(R.drawable.battery6);
                    break;
                default:
                    break;
            }
        }

    }


    abstract void updateUI(Datapacket dp);

    abstract void initUI();


    abstract int setID();
}
