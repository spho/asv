package com.example.christoph.asv;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;

public class RecordActivity extends SuperActivity {

    Button recordbutton;
    Button pauseButton;
    Button syncbutton;
    Button stopButton;
    TextView outputTextView;
    ImageView statusImage;
    File file;
    String logFileName = "logFile";

    boolean isRecording = false;

    FileOutputStream outputStream;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);
        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setCustomView(R.layout.myfancycustomheader);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        errorButton = (Button) findViewById(R.id.button15);


        View.OnTouchListener onTouchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getActionMasked() ==  MotionEvent.ACTION_UP) {
                    Log.d("Click", "Clicked on: " + v.toString());
                    if (v == (View) errorButton) {
                        sendToCordinator(4, 7, 1, null);
                    }
                    if (v == (View) recordbutton) {
                        sendToCordinator(7, 2, 1, null);
                    }
                    if (v == (View) stopButton) {
                        sendToCordinator(7, 2, 2, null);
                    }
                    if (v == (View) pauseButton) {
                        if (isRecording) {
                            sendToCordinator(7, 2, 2, null);
                            isRecording = false;
                            pauseButton.setText("Start");
                        } else {
                            sendToCordinator(7, 2, 3, null);
                            isRecording = true;
                            pauseButton.setText("Pause");
                        }
                    }
                    if (v == (View) syncbutton) {
                        sendToCordinator(7, 2, 4, null);
                    }
                }

                return true;
            }
        };


        errorButton.setOnTouchListener(onTouchListener);

        recordbutton = (Button) findViewById(R.id.button11);
        stopButton = (Button) findViewById(R.id.button12);
        pauseButton = (Button) findViewById(R.id.button14);
        syncbutton = (Button) findViewById(R.id.button13);

        recordbutton.setOnTouchListener(onTouchListener);
        stopButton.setOnTouchListener(onTouchListener);
        pauseButton.setOnTouchListener(onTouchListener);
        syncbutton.setOnTouchListener(onTouchListener);

        outputTextView = (TextView) findViewById(R.id.textView4);
        statusImage = (ImageView) findViewById(R.id.imageView3);
        statusImage.setImageResource(R.drawable.battery0);

        logFileName=findNewLogFile();
        pauseButton.setText("Start");


    }

    private void log(String str) {
        try {
            outputStream = openFileOutput(logFileName, Context.MODE_PRIVATE);
            outputStream.write(str.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String findNewLogFile(){
        String str = "logfile";
        int number = 1;
        File f = new File(getApplicationContext().getFilesDir()+str+number);
        while(f.exists()==false&&number<1000) {
            f = new File(getApplicationContext().getFilesDir() + str + number);
            number++;
        }

        return (getApplicationContext().getFilesDir()+str+number);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_record, menu);
        return true;
    }

    @Override
    int setID() {
        return 5;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        sendToCordinator(4, 7, 1, null);
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    void updateUI(Datapacket dp) {
        for (int i = 0; i < dp.dataArray.length; i++) {
            if (dp.dataArray[i].type == 15) {
                outputTextView.setText(dp.dataArray[i].integer + "");
            }
            if (dp.dataArray[i].type == 22) {
                if (dp.dataArray[i].list != null) {
                    String str = "";
                    for (int j = 0; j < dp.dataArray[i].list.size(); j++) {
                        str += dp.dataArray[i].list.get(j);
                    }
                    log(str);
                }
            }
        }


    }

    @Override
    void initUI() {

    }
}
