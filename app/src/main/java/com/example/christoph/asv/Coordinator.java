package com.example.christoph.asv;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Christoph on 24.09.2015.
 */
public class Coordinator implements Runnable {

    static public Handler mHandler;
    public Bluetoothhandler btHandler;
    private ASV asv;
    static private Handler uiHandler;
    private SuperActivity activeActivity;
    private Timer timer = new Timer();
    final int FPS = 1;
    TimerTask masterclock = new MasterClock(this,0);
    TimerTask sendclock = new MasterClock(this,1);
    public Context currentContext;
    boolean firstTick = true;

    boolean debug = false;


    private boolean test = true;

    public Coordinator(Context context) {

        //For Testing
        Testclass tester = new Testclass();
        Log.d("Tag", "to " + tester.toString());

        //End of Testing

        currentContext = context;
        asv = new ASV();
    }

    private void init() {


        masterclock = new MasterClock(this,0);
        timer.scheduleAtFixedRate(masterclock, 100, 1000 / FPS);
        sendclock = new MasterClock(this,1);
        timer.scheduleAtFixedRate(sendclock, 100, 300);
    }

    @Override
    public void run() {

        if (Looper.myLooper() == null) {
            Looper.prepare();
        }
        mHandler = new Handler() {
            public void handleMessage(Message msg) {
                receivedMsg(msg);

            }
        };
        init();
        mHandler.obtainMessage(4, 1, -1, null)
                .sendToTarget();
        Looper.loop();
    }

    private void changeUIscreen(int which) {
        Intent intent;
        switch (which) {
            case 1:
                intent = new Intent(currentContext, DriveActivity.class);
                break;
            case 2:
                intent = new Intent(currentContext, EnergyActivity.class);
                break;
            case 3:
                intent = new Intent(currentContext, SensorActivity.class);
                break;
            case 4:
                intent = new Intent(currentContext, DrivePlaningActivity.class);
                break;
            case 5:
                intent = new Intent(currentContext, RecordActivity.class);
                break;
            case 6:
                intent = new Intent(currentContext, DebugActivity.class);
                break;
            case 7:
                intent = new Intent(currentContext, MenuActivity.class);
                break;
            case 8:
                intent = new Intent(currentContext, SettingsActivity.class);
                break;
            default:
                return;

        }
        currentContext.startActivity(intent);


    }

    private void receivedMsg(Message msg) {
        // Log.d("COM", "Received something");
        try {
            switch (msg.what) {
                case 1:
                    Datapacket dp = (Datapacket) msg.obj;
                    interpretArrayValue(dp.dataArray);
                    if (msg.arg2 == 1 && msg.arg1 == 1) {
                        /*
                        byte[] bytes = new byte[25];

                        bytes[0] = '.';
                        bytes[1] = (byte) ((int)Math.max(-128,Math.min(127,(asv.inputSpeed  * Math.cos(asv.inputSteering * 2 * Math.PI/360)*1.27 + asv.inputSpeed  * Math.sin(asv.inputSteering * 2 * Math.PI/360)))));
                        bytes[2] = (byte) ((int)Math.max(-128,Math.min(127,(asv.inputSpeed  * Math.cos(asv.inputSteering * 2 * Math.PI/360)*1.27 + asv.inputSpeed  * Math.sin(-asv.inputSteering * 2 * Math.PI/360)))));
                        bytes[3] = 10;
                        bytes[4] = 13;
                        bytes[4] = 13;
                        bytes[4] = 13;



                        Log.d("Output","Speed: "+asv.inputSpeed+" angle: "+asv.inputSteering+" left: "+bytes[1]+" right: "+bytes[2] );

                            btHandler.sendBT(bytes);
                            */

                    }
                    break;
                case 2:
                    processUIRequest(msg.arg1);
                    break;
                case 3:
                    Log.d("Handler", "Wrong packet arrived with what value of 3");
                    break;
                case 4:
                    changeUIscreen(msg.arg1);
                    break;
                case 5:
                    //Timer tick inputi
                    if (activeActivity != null) {
                        if (firstTick) {
                            btHandler = new Bluetoothhandler(this);
                            firstTick = false;
                        }

                       // btHandler.sendRandomValuesToCoordinator();
                        processUIRequest(activeActivity.idOfActivity);
                    }
                    break;
                case 6:
                    activeActivity = (SuperActivity) msg.obj;
                    break;
                case 7:
                    //Send debug command to ASV or some else command, like form the recording activity
                    processReceivedCommand(msg.arg1, msg.arg2);
                    break;
                case 8:
                    //Send Toast to activity
                    Toast.makeText(activeActivity.getApplicationContext(), msg.obj.toString(), Toast.LENGTH_LONG).show();
                    break;
                case 9:

                    int scalingFactor =60;
                    float steeringDampening = 0.5f;

                    float torque_left=(((int)(scalingFactor*Math.max(-128,Math.min(127,(asv.inputSpeed  * Math.cos(asv.inputSteering * 2 * Math.PI/360)*1.27 + steeringDampening*asv.inputSpeed  * Math.sin(asv.inputSteering * 2 * Math.PI/360)))))));
                    float torque_right=(((int)(scalingFactor*(Math.max(-128,Math.min(127,(asv.inputSpeed  * Math.cos(asv.inputSteering * 2 * Math.PI/360)*1.27 + steeringDampening*asv.inputSpeed  * Math.sin(-asv.inputSteering * 2 * Math.PI/360))))))));

                    int logScaling = 500;
                    torque_left=Math.min(7500, Math.max((float) Math.log(1 + Math.abs(torque_left)/2) * Math.signum(torque_left) * logScaling + torque_left / 2, -7500));
                    torque_right=Math.min(7500,Math.max((float)Math.log(1+Math.abs(torque_right)/2)*Math.signum(torque_right)*logScaling+torque_right/2,-7500));;




                    byte[] bytes = new byte[24];
                    bytes[0] = '.';
                    bytes[1]=(byte)(((int)torque_left)>>8);
                    bytes[2]=(byte)(((int)torque_right)>>8);
                    bytes[3]=(byte)(((int)torque_left)&0x00FF);
                    bytes[4]=(byte)(((int)torque_right)&0x00FF);
                    bytes[5] = 10;

                    bytes[6] = '.';
                    bytes[7]=(byte)(((int)torque_left)>>8);
                    bytes[8]=(byte)(((int)torque_right)>>8);
                    bytes[9]=(byte)(((int)torque_left)&0x00FF);
                    bytes[10]=(byte)(((int)torque_right)&0x00FF);
                    bytes[11] = 10;

                    bytes[12] = '.';
                    bytes[13]=(byte)(((int)torque_left)>>8);
                    bytes[14]=(byte)(((int)torque_right)>>8);
                    bytes[15]=(byte)(((int)torque_left)&0x00FF);
                    bytes[16]=(byte)(((int)torque_right)&0x00FF);
                    bytes[17] = 10;

                    bytes[18] = '.';
                    bytes[19]=(byte)(((int)torque_left)>>8);
                    bytes[20]=(byte)(((int)torque_right)>>8);
                    bytes[21]=(byte)(((int)torque_left)&0x00FF);
                    bytes[22]=(byte)(((int)torque_right)&0x00FF);
                    bytes[23] = 10;


                    Log.d("Output","Speed: "+asv.inputSpeed+" angle: "+asv.inputSteering+" left: "+bytes[1]+" right: "+bytes[2] );
                    btHandler.sendBT(bytes);

                    break;

                default:
                    break;
            }
        } catch (Exception e) {
            Log.d("Exception", "Coordinator: recmsg try catch failed " + e.toString());
        }

    }

    private void processReceivedCommand(int type, int parameter) {

        switch (type) {
            case 1:

                if(debug){
                    asv.reply=""+parameter;
                }

                //Is for the debug activity
                byte[] bytes = new byte[24];
                bytes[0] = 1;
                bytes[1]=(byte)((((int)parameter)>>24)&0x000000FF);
                bytes[2]=(byte)((((int)parameter)>>16)&0x000000FF);
                bytes[3]=(byte)((((int)parameter)>>8)&0x000000FF);
                bytes[4]=(byte)(((int)parameter)&0x000000FF);
                bytes[5] = 1;

                bytes[6] = 1;
                bytes[7]=(byte)((((int)parameter)>>24)&0x000000FF);
                bytes[8]=(byte)((((int)parameter)>>16)&0x000000FF);
                bytes[9]=(byte)((((int)parameter)>>8)&0x000000FF);
                bytes[10]=(byte)(((int)parameter)&0x000000FF);
                bytes[11] = 1;

                bytes[12] = 1;
                bytes[13]=(byte)((((int)parameter)>>24)&0x000000FF);
                bytes[14]=(byte)((((int)parameter)>>16)&0x000000FF);
                bytes[15]=(byte)((((int)parameter)>>8)&0x000000FF);
                bytes[16]=(byte)(((int)parameter)&0x000000FF);
                bytes[17] = 1;

                bytes[18] = 1;
                bytes[19]=(byte)((((int)parameter)>>24)&0x000000FF);
                bytes[20]=(byte)((((int)parameter)>>16)&0x000000FF);
                bytes[21]=(byte)((((int)parameter)>>8)&0x000000FF);
                bytes[22]=(byte)(((int)parameter)&0x000000FF);
                bytes[23] = 1;


                Log.d("Output","Sent command: "+parameter);
                btHandler.sendBT(bytes);
                if(debug) {
                    asv.reply += ".";
                }
                break;
            case 2:
                //Is for the record activity
                switch (parameter) {
                    case 1:
                        //Record
                        break;
                    case 2:
                        //Stop
                        break;
                    case 3:
                        //Start
                        break;
                    case 4:
                        //Sync
                        break;
                    default:
                        break;
                }
                break;
            case 3:
                //Is for general commands
                break;
            default:
                break;
        }
    }

    private void processUIRequest(int type) {
        Datapacket dp = new Datapacket();

        switch (type) {
            case 1:
                dp = provideDataForView1();
                break;
            case 2:
                dp = provideDataForView2();
                break;
            case 3:
                dp = provideDataForView3();
                break;
            case 4:
                dp = provideDataForView4();
                break;
            case 5:
                dp = provideDataForView5();
                break;
            case 6:
                dp = provideDataForView6();
                break;

            default:
                break;
        }
        activeActivity.myHandler.obtainMessage(3, -1, -1, dp)
                .sendToTarget();



    }

    private Datapacket provideDataForView5() {

        Datapacket dp = new Datapacket();
        dp.addValue(5, asv.battery);
        dp.addValue(16, asv.isRecording);
        dp.addValue(17, asv.isSync);
        dp.addValue(15, asv.bufferFillRate);
        dp.addValue(23, asv.hasError);
        if (asv.recordings.get(0) != null) {
            dp.addValue(22, asv.recordings.get(0));
        }
        dp.fialiseArray();
        return dp;

    }

    private Datapacket provideDataForView4() {
        Datapacket dp = new Datapacket();
        dp.addValue(5, asv.battery);
        dp.addValue(23, asv.hasError);
        if(!debug) {
                dp.addValue(13,asv.positionASV,0);
            dp.addValue(13,asv.quadASV[0],1);
            dp.addValue(13,asv.quadASV[1],2);
            dp.addValue(13,asv.quadASV[2],3);
             for (int i = 0; i < asv.pathpoint.length; i++) {
               dp.addValue(13, asv.pathpoint[i], i);
             }
        }else {
            dp.addValue(13, new Pathpoint(40, 50), 0);
            dp.addValue(13, new Pathpoint(10, 0), 1);
            dp.addValue(13, new Pathpoint(30, 50), 2);
            dp.addValue(13, new Pathpoint(80, 90), 3);
            dp.addValue(13, new Pathpoint(10, 0), 4);
            dp.addValue(13, new Pathpoint(15, 20), 5);
            dp.addValue(13, new Pathpoint(25, 25), 6);
            dp.addValue(13, new Pathpoint(30, 35), 7);
            dp.addValue(13, new Pathpoint(40, 40), 8);
            dp.addValue(13, new Pathpoint(38, 45), 9);
            dp.addValue(13, new Pathpoint(40, 50), 10);
        }

        dp.fialiseArray();
        return dp;

    }

    private Datapacket provideDataForView6() {
        Datapacket dp = new Datapacket();
        dp.addValue(5, asv.battery);
        dp.addValue(19, asv.reply);
        dp.addValue(23, asv.hasError);
        dp.fialiseArray();
        return dp;
    }

    private Datapacket provideDataForView3() {
        Datapacket dp = new Datapacket();
        dp.addValue(5, asv.battery);
        dp.addValue(23, asv.hasError);
        for (int i = 0; i < asv.sensor.length; i++) {

            dp.addValue(12, asv.sensor[i], i);

        }
        dp.fialiseArray();
        return dp;

    }

    private Datapacket provideDataForView2() {
        Datapacket dp = new Datapacket();
        if(!debug) {
            dp.addValue(5, asv.battery);
            dp.addValue(6, asv.capacitor);
            dp.addValue(7, asv.pv);
            dp.addValue(8, asv.onBoardElectronics);
            dp.addValue(9, asv.powerM1);
            dp.addValue(10, asv.powerM2);
            dp.addValue(23, asv.hasError);
        }else {

            dp.addValue(6, 20f);
            dp.addValue(7, 30f);
            dp.addValue(8, (float)(40f));
            dp.addValue(9, 50f);
            dp.addValue(10, (float)(10+10f));
            dp.addValue(23, 70f);

        }
        dp.fialiseArray();
        return dp;
    }

    private Datapacket provideDataForView1() {
        Datapacket dp = new Datapacket();
        dp.addValue(5, asv.battery);
        dp.addValue(7, asv.pv);
        dp.addValue(8, asv.onBoardElectronics);
        dp.addValue(9, asv.powerM1);
        dp.addValue(10, asv.powerM2);
        dp.addValue(23, asv.hasError);
        dp.fialiseArray();
        return dp;
    }

    private void interpretArrayValue(ArrayValue[] ar) {
        //  Log.d("COM","At intepret ArrayValue "+ar.length);
        for (int i = 0; i < ar.length; i++) {
            switch (ar[i].type) {
                case 1:
                    asv.speedM1 = ar[i].floaty;
                    break;
                case 2:
                    asv.speedM2 = ar[i].floaty;
                    break;
                case 3:
                    asv.speedASV = ar[i].floaty;
                    break;
                case 4:
                    asv.angleASV = ar[i].floaty;
                    break;
                case 5:
                    asv.battery = ar[i].floaty;
                    break;
                case 6:
                    asv.capacitor = ar[i].floaty;
                    break;
                case 7:
                    asv.pv = ar[i].floaty;
                    break;
                case 8:
                    asv.onBoardElectronics = ar[i].floaty;
                    break;
                case 9:
                    asv.powerM1 = ar[i].floaty;
                    break;
                case 10:
                    asv.powerM2 = ar[i].floaty;
                    break;
                case 11:
                    if (asv.energyflow.length > ar[i].offset1) {
                        asv.energyflow[ar[i].offset1] = ar[i].floaty;
                    }
                    break;
                case 12:
                    if (asv.sensor.length > ar[i].offset1) {
                        asv.sensor[ar[i].offset1] = ar[i].floaty;
                    }
                    break;
                case 13:
                    if (asv.pathpoint.length > ar[i].offset1) {
                        asv.pathpoint[ar[i].offset1] = ar[i].pathp;
                    }
                    break;
                case 14:

                    //  asv.recordings.set(ar[i].offset1, ar[i].list);

                    break;
                case 15:
                    //asv.bufferFillRate = ar[i].integer;
                    asv.bufferFillRate = (int)ar[i].floaty;
                    break;
                case 16:
                    asv.isRecording = ar[i].bool;
                    break;
                case 17:
                    asv.isSync = ar[i].bool;
                    break;
                case 18:
                    asv.request = ar[i].text;
                    break;
                case 19:
                    asv.reply = ar[i].text;
                    break;
                case 20:
                    asv.inputSpeed = ar[i].floaty;
                    break;
                case 21:
                    asv.inputSteering = ar[i].floaty;
                    // Log.d("COM","Just for controlling that everything works nice and smoothly");
                    break;
                case 22:
                    //Type 22 is for passing an whole arrraylist from the coordinator to the recording activity

                    break;
                case 23:
                    asv.hasError = ar[i].bool;
                    break;
                case 24:
                    asv.positionASV=ar[i].pathp;
                    break;
                case 25:
                    asv.quadASV[ar[i].offset1]=ar[i].pathp;
                    break;
                case 26:
                    break;
                case 27:
                    break;
                case 28:
                    break;
                case 29:
                    break;
                case 30:
                    break;
                case 31:
                    break;
                case 32:
                    break;
                case 33:
                    break;
                case 34:
                    break;
                case 35:
                    break;
                case 36:
                    break;
                default:
                    break;
            }
        }
    }


}
