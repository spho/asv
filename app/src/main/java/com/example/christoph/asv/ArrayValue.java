package com.example.christoph.asv;

import java.util.ArrayList;

public class ArrayValue {

    public int type;
    public float floaty;
    public int integer;
    public boolean bool;
    public int offset1;
    public Pathpoint pathp;
    public String text;
    public ArrayList<Byte> list;



    public ArrayValue(int t) {
        type = t;

    }

}

