package com.example.christoph.asv;

/**
 * Created by Christoph on 08.10.2015.
 */


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

public class DynamicMeter extends View {

    private Rect rectangle;
    private Paint paint;
    int x = 0;
    int y = 0;
    int width= 0;
    int sideLength = 0;
    int mode = 0;


    public DynamicMeter(Context context,int value, int m) {
        super(context);

        mode=m;


        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;

        // create the Paint and set its color
        paint = new Paint();
        if(mode==1) {
            paint.setColor(Color.GREEN);
        }if(mode==2){
            if(sideLength>x){
                paint.setColor(Color.rgb(255,120,0));
            }else {
                paint.setColor(Color.GREEN);
            }
        }

        if(mode==1){
            x=0;
        }else if(mode==2){
            x= width/2;
        }

        sideLength = (int)((float)(value)/100*(width));


        // create a rectangle that we'll draw later
        if(x>sideLength){
            int t = x;
            x = sideLength;
            sideLength = t;
        }
        rectangle = new Rect(x, y, sideLength,300 );


    }

    //Value form 0 to 100
    public void update(int value){

        if(mode==1){
            x=0;
        }else if(mode==2){
            x= width/2;
        }

        if(mode==1) {
            paint.setColor(Color.GREEN);
        }if(mode==2){
            if(sideLength>x){
                paint.setColor(Color.rgb(255,120,0));
            }else {
                paint.setColor(Color.GREEN);
            }
        }
        sideLength = (int)((float)(value)/100*(width));

        if(x>sideLength){
            int t = x;
            x = sideLength;
            sideLength = t;
        }

        rectangle = new Rect(x, y, sideLength, 300);



    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawColor(Color.GRAY);
        canvas.drawRect(rectangle, paint);
    }

}