package com.example.christoph.asv;

import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;

public class Datapacket {

    public ArrayValue[] dataArray;
    private ArrayList<ArrayValue> bufferArray = new ArrayList<ArrayValue>();

    public Datapacket() {

    }

    public void addValue(int type, float f) {
        ArrayValue ar = new ArrayValue(type);
        ar.floaty = f;
        bufferArray.add(ar);
    }

    public void addValue(int type, int i) {
        ArrayValue ar = new ArrayValue(type);
        ar.integer = i;
        bufferArray.add(ar);
    }

    public void addValue(int type, boolean b) {
        ArrayValue ar = new ArrayValue(type);
        ar.bool = b;
        bufferArray.add(ar);
    }

    public void addValue(int type, String s) {
        ArrayValue ar = new ArrayValue(type);
        ar.text = s;
        bufferArray.add(ar);
    }

    public void addValue(int type, Pathpoint p, int offset) {
        ArrayValue ar = new ArrayValue(type);
        ar.pathp = p;
        ar.offset1 = offset;
        bufferArray.add(ar);
    }

    public void addValue(int type, float f, int offset) {
        ArrayValue ar = new ArrayValue(type);
        ar.floaty = f;
        ar.offset1 = offset;
        bufferArray.add(ar);
    }

    public void addValue(int type, ArrayList<Byte> arl) {
        ArrayValue ar = new ArrayValue(type);
        ar.list = arl;
        bufferArray.add(ar);
    }


    public void fialiseArray() {
        dataArray = bufferArray.toArray(new ArrayValue[bufferArray.size()]);
    }

    public void decodeByteArray(byte[] array) {
        if (array != null&&array.length!=0) {
            ArrayList<ArrayValue> buffer = new ArrayList<ArrayValue>();
            ArrayValue[] arrayValues = null;
            int nrOfStartingbytes = 1;
            int base = -1;
            int lenght = -1;
            for(int i =0;i<array.length-nrOfStartingbytes;i++) {
                try {
                    if (array[i] == 46 && array[array[i + 1] + i+2] == 10) {
                        base = i;
                        lenght = array[i + 1];
                    }
                } catch (Exception e) {

                }
            }
            if(lenght==-1||lenght<1){

            }
                for(int i = base+2;i<lenght+base;){
                    //Case for decoding an float
                    if((array[i]>0&&array[i]<11)||array[i]==15||array[i]==20||array[i]==21) {
                        ArrayValue ar = new ArrayValue(array[i]);
                        byte[] bytes = new byte[4];
                        bytes[0] = array[i + 1];
                        bytes[1] = array[i + 2];
                        bytes[2] = array[i + 3];
                        bytes[3] = array[i + 4];
                        float f = ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN).getFloat();
                        ar.floaty = f;
                        buffer.add(ar);
                        i=i+5;
                    }
                    //Case for decoding an bool
                   else if(array[i]==16||array[i]==17||array[i]==23){
                        ArrayValue ar = new ArrayValue(array[i]);
                        if(array[i+1]==0){
                            ar.bool=false;
                        }else{
                            ar.bool=true;
                        }
                        buffer.add(ar);
                        i=i+2;
                    }


                    //Case for decoding an string
                   else if(array[i]==18||array[i]==19){
                        int strinlenght = array[i+1];
                        String text = "";
                        ArrayValue ar = new ArrayValue(array[i]);

                        byte[] bufferForBytes = new byte[strinlenght];
                        for(int j = 0;j<strinlenght;j++){
                            text+= (char)array[i+2+j];
                        }
;
                        ar.text=text;
                        buffer.add(ar);
                        i=i+2+strinlenght;
                    }

                    //Case for decoding an float with offset
                   else if(array[i]==11||array[12]==12) {
                        ArrayValue ar = new ArrayValue(array[i]);
                        byte[] bytes = new byte[4];
                        bytes[0] = array[i + 1];
                        bytes[1] = array[i + 2];
                        bytes[2] = array[i + 3];
                        bytes[3] = array[i + 4];
                        int offset = array[i+5];
                        float f = ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN).getFloat();
                        ar.floaty = f;
                        ar.offset1=offset;
                        buffer.add(ar);
                        i=i+6;
                    }

                    //Case for decoding an pathpoint
                   else if(array[i]==13){
                        ArrayValue ar = new ArrayValue(array[i]);
                        Pathpoint pp = new Pathpoint();
                        pp.x=array[i+1];
                        pp.y=array[i+2];
                        ar.pathp=pp;
                        ar.offset1=array[i+3];
                        buffer.add(ar);
                        i=i+4;
                    }

                    //Case for decoding an asvPosition
                  else  if(array[i]==24){
                        ArrayValue ar = new ArrayValue(array[i]);
                        Pathpoint pp = new Pathpoint();
                        pp.x=array[i+1];
                        pp.y=array[i+2];
                        ar.pathp=pp;
                        ar.offset1=array[i+3];
                        buffer.add(ar);
                        i=i+4;
                    }

                    //Case for decoding an quadASV
                   else if(array[i]==25){
                        ArrayValue ar = new ArrayValue(array[i]);
                        Pathpoint pp = new Pathpoint();
                        pp.x=array[i+1];
                        pp.y=array[i+2];
                        ar.pathp=pp;
                        ar.offset1=array[i+3];
                        buffer.add(ar);
                        i=i+4;
                    }
                    else{
                        i=lenght+base;
                    }

                }


            /*

            byte[] bytebuff = new byte[4];
            int i = 0;
            byte startseq = 67;
            int lenght = 0;

            while (i + 10 < array.length && array[i] != startseq) {
                i++;
            }
            int j = i;
            i++;
            try {
                ArrayValue v = new ArrayValue(100); //Type
                v.integer = array[i];
                buffer.add(v);
                v = new ArrayValue(101); //Sequence
                v.integer = array[i + 1];
                buffer.add(v);
                v = new ArrayValue(102);//Lenght
                v.integer = array[i + 2];
                buffer.add(v);
                for (int k = 0; 4 * k + 5 < array[i + 2] - 4; k++) {

                    v = new ArrayValue(array[i + 4 + k * 4]);
                    for (int l = 0; l < 4; l++) {
                        bytebuff[l] = array[i + 5 + k * 4 + l];
                    }
                    float g = ByteBuffer.wrap(bytebuff).order(ByteOrder.LITTLE_ENDIAN).getFloat();
                    v.floaty = g;
                }
                byte checksum = array[i + array[i + 2]];
                byte test = 0;
                for (int g = 0; g < array[i + 2]; g++) {
                    test = (byte) ((int) test ^ (int) array[i + g]);
                }
                if (checksum == test) {
                    dataArray = buffer.toArray(new ArrayValue[buffer.size()]);
                } else {
                    dataArray = null;
                }
                return dataArray;

            }catch (Exception e){
                Log.d("Datapacket", "Error at decoding bytearray: "+e.toString());
            }
        }
        */
            arrayValues=buffer.toArray(new ArrayValue[buffer.size()]);
            dataArray=  arrayValues;

        }

    }

}

