package com.example.christoph.asv;

/**
 * Created by Christoph on 08.10.2015.
 */

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

public class PathDrawingView extends View {


    private Rect rectangle;
    private Paint paint;
    private Paint nicepaint;
    private Paint redpaint;

    int width = 0;
    int heght = 0;

    int xGreenDot=0;
    int yGreenDot=0;

    int x1 = 0;
    int x2 = 0;
    int y1 = 0;
    int y2 = 0;
    int x3=0;
    int y3=0;

    int seizeX = 50;
    int seizeY = 70;
    int rotation = 0;

    float[] points = new float[1000];
    Point startPoint = new Point();

    public PathDrawingView(Context context) {
        super(context);
        initPathDrawingView(context);
    }

    public PathDrawingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initPathDrawingView(context);
    }

    public PathDrawingView(Context context, AttributeSet attrs, int defaultStyle) {
        super(context, attrs, defaultStyle);
        initPathDrawingView(context);
    }


    public void initPathDrawingView(Context context) {


        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        heght = size.y;

        startPoint = new Point(width / 2, (int) (heght * 0.8));

        // create the Paint and set its color
        paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(5);
        paint.setAntiAlias(true);

        nicepaint = new Paint();
        nicepaint.setColor(Color.GREEN);
        nicepaint.setAntiAlias(true);


        redpaint = new Paint() {
            {
                setStyle(Paint.Style.STROKE);
                setStrokeCap(Paint.Cap.ROUND);
                setStrokeWidth(5.0f);
                setAntiAlias(true);
            }
        };
        redpaint.setColor(Color.RED);



        rectangle = new Rect((int) (float) (width) / 2 - seizeX / 2, 0, (int) ((float) (width) / 2 + seizeX / 2), seizeY);

        /*
        float[] test = new float[12];
        test[0] = 0;
        test[1] = 0;
        test[2] = 300;
        test[3] = 100;
        test[4] = 300;
        test[5] = 100;
        test[6] = 400;
        test[7] = 500;
        test[8] = 400;
        test[9] = 500;
        test[10] = 600;
        test[11] = 600;
        update(test,0);
*/


    }

    //Value form 0 to 100
    public void update(float[] p, int rotationOfV) {

        if(p.length<8){
            return;
        }
        points = p;



        for (int i = 0; i < points.length; i = i + 2) {
            points[i] = points[i] / 120 * width;
            points[i + 1] = (float) ((points[i + 1] / 150 * (heght)));
        }
        xGreenDot=(int) points[0];
        yGreenDot=(int) points[1];

        x1 =  (int)points[4];
        y1 =  (int)points[5];
        x2 =  (int)points[8];
        y2 =  (int)points[9];
        x3=(int) points[12];
        y3= (int)points[13];

        rotation = rotationOfV;

    }

    @Override
    protected void onDraw(Canvas canvas) {
        //canvas.drawColor(Color.GRAY);

       // int x1 = width / 2;
        //int y1 = 0;
        //int x3 = (int) (width * 0.75);
        //int y3 = (int) (heght * 0.4);

        Path path = new Path();
        path.moveTo(x1, y1);

       //float x2 = (float) (5);
        //float y2 = (float) (heght * 0.9);
        path.quadTo(x2, y2, x3, y3);
       canvas.drawPath(path, redpaint);



        canvas.drawLines(points, 16, points.length-16, paint);
        //Add dependencie
       // canvas.drawCircle(width / 2 - width / 25, width / 25, width / 25, nicepaint);
        canvas.drawCircle(xGreenDot,yGreenDot, width / 25, nicepaint);

    }

}
