package com.example.christoph.asv;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SensorActivity extends SuperActivity {

    TextView textView;
    final int numberofRows = 5;
    float[] values = new float[numberofRows];
    String[] prefix = new  String[numberofRows];
    String[] postfix = new String[numberofRows];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor);

        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setCustomView(R.layout.myfancycustomheader);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        errorButton = (Button) findViewById(R.id.button15);


        View.OnTouchListener onTouchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getActionMasked() ==  MotionEvent.ACTION_UP) {
                    Log.d("Click", "Clicked on: " + v.toString());
                    if (v == (View) errorButton) {
                        sendToCordinator(4, 7, 1, null);
                    }
                }
                return true;
            }
        };
        errorButton.setOnTouchListener(onTouchListener);
        textView = (TextView) findViewById(R.id.textView5);

        initUI();

        updateUI(null);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sensor, menu);
        return true;
    }

    private String composeText() {
        String str = new String();

        for (int i = 0; i < numberofRows; i++) {
            str += prefix[i] + values[i]+postfix[i] + "\n";
        }
        return str;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        sendToCordinator(4, 7, 1, null);

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    void updateUI(Datapacket dp) {
        try {
            for (int i = 0; i < dp.dataArray.length; i++) {
                if (dp.dataArray[i].type == 12&&dp.dataArray[i].offset1<values.length) {
                    values[dp.dataArray[i].offset1] = dp.dataArray[i].floaty;
                }
            }
            textView.setText(composeText());
        }
        catch (Exception e){

        }
    }

    @Override
    int setID() {
        return 3;
    }

    @Override
    void initUI() {
        for(int i = 0;i<numberofRows;i++){
            prefix[i]=new String();
            postfix[i]= "swag";
        }

        prefix[0]="Ultrasonic 1: ";
        prefix[1]="Voltage 5V: ";
        prefix[2]="Current: ";
        prefix[3]="SWAG 3: ";
        prefix[4]="LIDAR: ";


        for(int i = 0;i<numberofRows;i++){
            values[i]=i*3.25f;
        }

    }
}
