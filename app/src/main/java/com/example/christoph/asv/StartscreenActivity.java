package com.example.christoph.asv;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.nio.ByteBuffer;

public class StartscreenActivity extends AppCompatActivity {

    private Thread cordinatorThread = new Thread();
    private Coordinator coordinator;
    private Handler resultHandler;

    boolean debug = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startscreen);

        resultHandler = new Handler() {
            public void handleMessage(Message msg) {
                if (msg.obj == null) {
                    //exit loading screen
                }


            }
        };
        if (debug) {
            byte[] bytes = new byte[100];
            byte[] floatbuffer = new byte[4];
            floatbuffer=  ByteBuffer.allocate(4).putFloat(23.5f).array();
            bytes[0] = 55; //crap
            bytes[1] = 46; //Start
            bytes[2] = 26;  //Nr of bytes
            bytes[3] = 20; //Float 20
            bytes[4] =floatbuffer[0];
            bytes[5] =floatbuffer[1];
            bytes[6] =floatbuffer[2];
            bytes[7] =floatbuffer[3];
            bytes[8] =16; //Bool 16
            bytes[9] =0;
            bytes[10] =17; //Bool 17
            bytes[11] =1;
            bytes[12] =24; //Pathpoint 24
            bytes[13] =5;
            bytes[14] =6;
            bytes[15] =2;
            bytes[16] =18;
            bytes[17] =5;
            bytes[18] ='H';
            bytes[19] ='E';
            bytes[20] ='L';
            bytes[21] ='L';
            bytes[22] ='O';
            bytes[23] =11; // Float with offset
            bytes[24] =44;
            bytes[25] =12;
            bytes[26] =2;
            bytes[27] =1;
            bytes[28] =5;
            bytes[29] =10; //End byte


            Datapacket dp = new Datapacket();
            dp.decodeByteArray(bytes);
            dp.toString();

        } else {
             coordinator = new Coordinator(this);
             cordinatorThread = new Thread(coordinator);
             cordinatorThread.start();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_startscreen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
