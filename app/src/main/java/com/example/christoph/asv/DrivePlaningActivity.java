package com.example.christoph.asv;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

public class DrivePlaningActivity extends SuperActivity {

    PathDrawingView pathDrawingView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drive_planing);

        initUI();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_drive_planing, menu);
        return true;
    }

    @Override
    int setID() {
        return 4;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);


    }

    @Override
    void updateUI(Datapacket dp) {
         ArrayList<Float> list = new ArrayList<Float>();
    for(int i =0;i<dp.dataArray.length;i++){
        if(dp.dataArray[i].type==13&&dp.dataArray[i].pathp!=null){
            list.add(dp.dataArray[i].pathp.x);
            list.add(dp.dataArray[i].pathp.y);
            list.add(dp.dataArray[i].pathp.x);
            list.add(dp.dataArray[i].pathp.y);

        }
    }
        list.remove(0);
        list.remove(0);

        list.remove(list.size()-1);
        list.remove(list.size()-1);



        float[] retruner = new float[list.size()];
        for(int i = 0;i<list.size();i++){
            retruner[i]=list.get(i);
        }


        pathDrawingView.update(retruner, 0);

        pathDrawingView.invalidate();
    }

    @Override
    void initUI() {
        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setCustomView(R.layout.myfancycustomheader);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.LTGRAY));
        errorButton = (Button) findViewById(R.id.button15);
        driveButton = (Button) findViewById(R.id.button17);
        driveplaningButton = (Button) findViewById(R.id.button9);
        batteryButton = (Button) findViewById(R.id.button16);


        View.OnTouchListener onTouchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getActionMasked() == MotionEvent.ACTION_UP) {
                    Log.d("Click", "Clicked on: " + v.toString());
                    if (v == (View) errorButton) {
                        sendToCordinator(4, 6, 1, null);
                    }
                    if (v == (View) batteryButton) {
                        sendToCordinator(4, 2, 1, null);
                    }
                    if (v == (View) driveplaningButton) {
                        sendToCordinator(4, 4, 1, null);
                    }
                    if (v == (View) driveButton) {
                        sendToCordinator(4, 1, 1, null);
                    }
                }

                    return true;
                }

        };
        errorButton.setOnTouchListener(onTouchListener);
        batteryButton.setOnTouchListener(onTouchListener);
        driveButton.setOnTouchListener(onTouchListener);
       // driveplaningButton.setOnTouchListener(onTouchListener);

        pathDrawingView = (PathDrawingView) findViewById(R.id.pathplaningview);
    }
}
