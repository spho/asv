package com.example.christoph.asv;

public class Pathpoint {

    public float x = 0;
    public float y = 0;

    public Pathpoint() {
    }


    public Pathpoint(float xx, float yy) {
        x = xx;
        y = yy;
    }

}