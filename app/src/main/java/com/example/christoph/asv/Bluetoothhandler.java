package com.example.christoph.asv;

import java.io.IOException;
import java.util.Random;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import java.io.OutputStream;
import java.io.InputStream;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import android.util.Log;


public class Bluetoothhandler {

    private BluetoothAdapter mBluetoothAdapter = null;
    private BluetoothSocket btSocket = null;
    private java.io.OutputStream outStream = null;
    private java.io.InputStream inStream = null;
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private boolean isAdapterActive = false;
    private boolean isBluetoothConAct = false;

    private Coordinator coordinator;
    private ConnectedThread activeConnection;

    // ==> hardcode your server's MAC address here <==7
    //From Holger with the actual device
    private static String address = "00:06:66:43:44:71";

    //Testing with arduino and nRF24l01
    //Did not work because nRF24l01 is not BT 4.0 LE so its not possible to connect to it.
   // private static String address = "F0:F0:C3:C4:C5:C6";


    public boolean isBluetoothAdapterActive() {
        return isAdapterActive;
    }

    public boolean isBluetoothConnectionAlive() {
        return isBluetoothConAct;
    }

    public void sendBT(byte[] msg){
        if(activeConnection!=null){
            activeConnection.write(msg);
        }

    }



    public Bluetoothhandler(Coordinator cord) {

        coordinator = cord;

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (mBluetoothAdapter == null) {
            Log.d("Bluetoothhandler", "Adapter is null");
            coordinator.mHandler.obtainMessage(8, -1, -1, "Bluetoothadapter is null")
                    .sendToTarget();
            return;
        }

        if (!mBluetoothAdapter.isEnabled()) {


            Log.d("Bluetoothhandler", "Adapter is not enabled");
            coordinator.mHandler.obtainMessage(8, -1, -1, "Bluetoothadapter is disabled")
                    .sendToTarget();

            //Must be encapsulated in a DP and sent to the activity
           // Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            //startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);



            return;
        }

        startCom();


    }

    public void sendRandomValuesToCoordinator(){

        Random random = new Random();
        Datapacket dp = new Datapacket();
        for(int i = 0;i<20;i++){
            dp.addValue(i,random.nextFloat()*100);
        }
        for(int i = 0;i<5;i++){
            dp.addValue(12,random.nextFloat(),i);
        }
        dp.addValue(19,"Hello my friend");
        dp.addValue(15,(int)6);

        for(int i = 0;i<40;i++){
            dp.addValue(13,new Pathpoint(random.nextFloat()*100,random.nextFloat()*100),i);
        }

        if(random.nextFloat()>0.5){
            dp.addValue(23,true);
        }else{
            dp.addValue(23,false);
        }

        dp.fialiseArray();
        coordinator.mHandler.obtainMessage(1, -1, -1, dp).sendToTarget();

    }

    public void startCom(){


            coordinator.mHandler.obtainMessage(8, -1, -1, "Bluetoothadapter is trying to connect")
                    .sendToTarget();
            ConnectThread ct = new ConnectThread(address);
            ct.run();



    }
/*

The ConnectThread and the ConnectedThread class is directly from:
http://developer.android.com/guide/topics/connectivity/bluetooth.html
(18.01.2016)
Is under Creative Common 2.5 Licence

Little personal adaption made

 */
    //From android example
    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;

        public ConnectThread(String addr) {
            // Use a temporary object that is later assigned to mmSocket,
            // because mmSocket is final
            BluetoothSocket tmp = null;
            BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(addr);

            mmDevice = device;


            // Get a BluetoothSocket to connect with the given BluetoothDevice
            try {
                // MY_UUID is the app's UUID string, also used by the server code
                tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
            } catch (IOException e) {
                coordinator.mHandler.obtainMessage(8, -1, -1, "Bluetoothadapter has failed to init the socket")
                        .sendToTarget();
            }
            mmSocket = tmp;
        }

        public void run() {
            // Cancel discovery because it will slow down the connection
            mBluetoothAdapter.cancelDiscovery();

            try {
                // Connect the device through the socket. This will block
                // until it succeeds or throws an exception
                mmSocket.connect();
            } catch (IOException connectException) {
                coordinator.mHandler.obtainMessage(8, -1, -1, "Bluetoothadapter was unable to connect"+connectException.toString())
                        .sendToTarget();
                // Unable to connect; close the socket and get out
                try {
                    mmSocket.close();
                } catch (IOException closeException) {
                    coordinator.mHandler.obtainMessage(8, -1, -1, "Bluetoothadapter was unable to close the failed connection")
                            .sendToTarget();
                }
                return;
            }

            // Do work to manage the connection (in a separate thread)
            ConnectedThread conT = new ConnectedThread(mmSocket);
            conT.start();
            activeConnection = conT;


        }

        /**
         * Will cancel an in-progress connection, and close the socket
         */
        public void cancel() {
            coordinator.mHandler.obtainMessage(8, -1, -1, "Bluetoothadapter is canceling the connection attempt")
                    .sendToTarget();
            try {
                mmSocket.close();
            } catch (IOException e) {
                coordinator.mHandler.obtainMessage(8, -1, -1, "Bluetoothadapter was unable to close the socket")
                        .sendToTarget();
            }
        }
    }

    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            coordinator.mHandler.obtainMessage(8, -1, -1, "Bluetoothadapter is connected")
                    .sendToTarget();
            Log.d("BT",socket.toString());
            // Get the input and output streams, using temp objects because
            // member streams are final
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                coordinator.mHandler.obtainMessage(8, -1, -1, "Bluetoothadapter has failed to get the input and output streams")
                        .sendToTarget();
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            byte[] buffer = new byte[1024];  // buffer store for the stream
            Datapacket dp = new Datapacket(); //Datapacket zum datentransport
            int bytes; // bytes returned from read()

            // Keep listening to the InputStream until an exception occurs
            while (true) {
                try {
                    // Read from the InputStream
                    bytes = mmInStream.read(buffer);
                    // Send the obtained bytes to the UI activity
                    if(bytes>0) {
                        Log.d("BT", "Received some data form the BT");
                        dp.decodeByteArray(buffer);
                        coordinator.mHandler.obtainMessage(1, -1, -1, dp).sendToTarget();
                    }
                } catch (IOException e) {
                    coordinator.mHandler.obtainMessage(8, -1, -1, "Bluetoothadapter had an error at receiving data")
                            .sendToTarget();
                    break;
                }
            }
        }



        /* Call this from the main activity to send data to the remote device */
        public void write(byte[] bytes) {
            try {

                if(mmOutStream!=null) {
                    mmOutStream.write(bytes);
                    mmOutStream.flush();

                }
            } catch (IOException e) {
                Log.d("BT","Error at sending some bytes: "+e.toString());
                coordinator.mHandler.obtainMessage(8, -1, -1, "Bluetoothadapter had an error sending data")
                        .sendToTarget();
                cancel();
            }
        }

        /* Call this from the main activity to shutdown the connection */
        public void cancel() {
            coordinator.mHandler.obtainMessage(8, -1, -1, "Bluetoothadapter is closing")
                    .sendToTarget();
            try {
                mmSocket.close();
                activeConnection = null;
            } catch (IOException e) {
                coordinator.mHandler.obtainMessage(8, -1, -1, "Bluetoothadapter has failed to close")
                        .sendToTarget();
            }
        }
    }

    /*
    End of code form
    http://developer.android.com/guide/topics/connectivity/bluetooth.html
     */


}
